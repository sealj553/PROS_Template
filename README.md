# PROS_Template
Getting started guide for using the [PROS library](https://pros.cs.purdue.edu) in C++ on the command line

I haven't tested anything here which involves the actual cortex because I don't have one. I'll probably update this later.

# Prerequisites/Setup
  * linux or linux-like environment with:
    * `git`
    * `make`
    * `gcc-arm-none-eabi`
    * `python3`
    * `python3-pip`


  * Install [pros-cli](https://github.com/purduesigbots/pros-cli) to upload firmware and initiate terminal sessions with the cortex

  ```
git clone https://github.com/purduesigbots/pros-cli.git
pip install -e .
```
  * Download [PROS](https://github.com/purduesigbots/pros) 
(doesn't compile without modification) or clone this repo (actually works, but might become outdated in the future)

  ```
cd pros
./build.sh
cd template
```

  or

  ```
git clone https://github.com/sealj553/PROS_Template.git
cd PROS_Template
```

# Writing code for your project
Inside the `include` directory are `API.h` and `main.h`. `API.h` includes all the important declarations for interfacing with the hardware. The comments in it are very useful. I would recommend reading through it and using it as a reference. It shouldn't be included directly in your files.

Information about using the API for manipulating digital pins, using I2C, interfacing with sensors, etc. can be found on the [PROS API page](https://pros.cs.purdue.edu/api/)

`main.h` should be included in every file which uses the PROS library. It includes `API.h` and contains declarations for important functions that you will implement. PROS encourages you to modify `main.h` by including your own header files so you don't have to include them separately. Doesn't seem like a very good practice to me...

There are a number of important functions in `main.h` which are automatically called by PROS when the robot boots. You will implement the definitions of these functions to make the robot do robot things.
Those functions include `autonomous()`, `initializeIO()`, `initialize()`,  and `operatorControl()`. For the specifics of what should be in these functions and when they are called, see `include/main.h`.

All the important files are in the `src` directory. That's where you will edit/create files. 

  * `init.c` contains the definitions for `initializeIO()` and `initialize()`
  *  `auto.c` contains the definition for `autonomous()`
  * `opcontrol.c` contains the definition for `operatorControl()`

Inside `src` you can create whatever you want to be called from any of the above implementation files.

You don't need to mess with the `Makefile` in there, or the other one in the parent directory. They will automatically find all the source files.



# Building your project
  * Enter your project directory (not `src`)
  * Run `make`
  * A `bin` directory will be created for the object files and executable
  
You can run `make` inside the `src` directory, but it will only compile the project, not link it.

# Uploading to the cortex
  * Compile your project
  * Enter the project directory
  * Use `pros-cli` to flash the firmware to the cortex
  (untested)

  ```
pros flash
```

# Creating terminal session
This will allow you to communicate via serial with the cortex. If you're debugging with printf, you need to do this to see the output.

  * Use `pros-cli` to create the terminal interface (untested)

  ```
pros terminal
```

# Using C++
I've modified the template to use C++, and it looks like it works fine.


The modifications I've made are:
  * Changing the files in `src` to .cpp
  * Adding `-Wno-builtin-declaration-mismatch` to `CPPFLAGS` in common.mk to remove warnings about standard library functions being redeclared by PROS in `API.h`
  * Calling `__libc_init_array()` in `initializeIO()` as described [here](https://web.archive.org/web/20171027055841/https://blog.pxtst.com/using-c-in-pros/) to enable static constructors being called
